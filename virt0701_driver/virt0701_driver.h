#ifndef VIRT0701_DRIVER_H__
#define VIRT0701_DRIVER_H__
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/uaccess.h>
#include <linux/io.h>
#include <linux/cdev.h>
#include <linux/list.h>
#include <linux/slab.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <linux/device.h>
#include <linux/err.h>
#include <linux/mdio.h>
#include <linux/phy.h>
#include <linux/platform_device.h>
#include <linux/of_address.h>
#include <linux/of_platform.h>
#include <linux/kdev_t.h>
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/regulator/driver.h>
#include <linux/regulator/machine.h>


typedef struct virt_regulator_dev_s
{	
	char name[32];
	struct i2c_client *client;
	struct regmap *regmap;
}virt_regulator_dev_t;


typedef struct virt_regulator_plat_data_s
{
	struct regmap *regmap;
	struct regulator_init_data *init_data;
}virt_regulator_plat_data_t;

#endif
